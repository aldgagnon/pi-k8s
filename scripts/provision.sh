#!/bin/bash

# Install Docker
echo "## Install Docker"
curl -sSL get.docker.com | sh
sudo usermod pi -aG docker

# Update respositories and upgrade software
echo "## Update & Upgrade"
sudo apt-get update -y
sudo apt-get upgrade -y

# Enable bridge traffic to be forwarded to iptables
# See more here: https://github.com/kubernetes/kubeadm/issues/312
echo "## Enable bridged traffic to be forwarded to iptables"
sudo sysctl net.bridge.bridge-nf-call-iptables=1

# Disable swap, required in Kubernetes > 1.7
echo "## Disable swap"
sudo dphys-swapfile swapoff && \
sudo dphys-swapfile uninstall && \
sudo update-rc.d dphys-swapfile remove
sudo systemctl disable dphys-swapfile

# Install Kubernetes
echo "## Install Kubernetes"
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add - && \
echo "deb http://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list && \
sudo apt-get update -q && \
sudo apt-get install -qy kubeadm

# Enable cgroup memory
echo "## Enable cgroup memory"
orig="$(head -n1 /boot/cmdline.txt) cgroup_enable=cpuset cgroup_memory=1 cgroup_enable=memory"
echo $orig | sudo tee /boot/cmdline.txt

# Reboot!
echo "## Rebooting"
sudo shutdown -r now