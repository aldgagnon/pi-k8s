# pi-k8s

A simple two node, Raspberry Pi powered Kubernetes cluster. 

# Hardware

| # | Item | Use | Quantity	|
|-|-|-|-|
| 1 | Raspberry Pi 4 - 4 GB RAM | Kubernetes Nodes | 2  | 
| 2 | SanDisk 32 GB microSD | Storage | 1  |
| 3 | Stackle case | Physical mount | 1 |

# Infrastructure
 - master: 192.168.0.150
 - worker: 192.168.0.151

# Installation steps

1. Download and run `provision.sh`
2. Change hostname
    - `sudo raspi-config`
      - master: `k8s-pi-master-0`
      - worker: `k8s-pi-worker-0`
3. Set a static IP and save the file:
    - `sudo vi /etc/dhcpcd.conf`
    - Paste the following block
4. Reboot
       
```
interface eth0
static ip_address=192.168.0.150/24
static routers=192.168.0.1
static domain_name_servers=8.8.8.8
```

# Master node

1. Pull the required Docker images
    - `kubeadm config images pull -v3`

2. Using WeaveNet
    - `kubeadm init --token-ttl=0 --apiserver-cert-extra-sans=aldgagnon.hopto.org --apiserver-advertise-address=192.168.0.150`
    - `kubectl apply -f "https://cloud.weave.works/k8s/net?k8s-version=$(kubectl version | base64 | tr -d '\n')"`

3. Verify

```
 $ kubectl get nodes
NAME              STATUS   ROLES    AGE   VERSION
k8s-pi-master-0   Ready    master   28m   v1.17.3
k8s-pi-worker-0   Ready    <none>   13m   v1.17.3
```

# Worker node

1. Connect to the cluster
    - `kubeadm join 192.168.0.150:6443 --token r6ddso.v026t34fdp3lrppo --discovery-token-ca-cert-hash sha256:359ae37855ae14086c9c567e79c134943b166d389fe8f815475158876387739d`

# Testing

```
$ kubectl apply -f https://k8s.io/examples/admin/dns/dnsutils.yaml
pod/dnsutils created
```

```
$ kubectl get pods
NAME       READY   STATUS    RESTARTS   AGE
dnsutils   1/1     Running   0          37s
```

# References
 - https://dev.to/anton2079/kubernetes-k8s-private-cloud-with-raspberry-pi-4s-k0d
 - https://github.com/teamserverless/k8s-on-raspbian/blob/master/GUIDE.md

